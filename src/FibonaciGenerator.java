import java.util.Arrays;

/**
 * fibonazi numbers are a sequence f(x)=f(x-2)+f(x-1). Where every number is the
 * sum of the two previous numbers the sequence looks like this: 1, 2, 3, 5, 8
 * .... for instance 8 is calculated as the sum of the two previous: (3 + 5)
 *
 */
public class FibonaciGenerator {
	public static void main(String[] args) {
		// final means that the value once assigned in the variable cannot be changed.
		// It's basically a constant.
		final int ARRAY_SIZE = 10;
		// we create an empty array with as much positions as we want
		int myFibonaziList[] = new int[ARRAY_SIZE];

		// this is a bit barroque, but it means, the variable i will start with 0 and
		// end with ARRAY_SIZE and the code inside will be executed every time we add 1
		// to the variable 1
		for (int i = 0; i < ARRAY_SIZE; i++) {
			if (i < 2) {
				// The first two numbers of the fibonaci list are 1. It's just so.
				// This means that the positions 0 and 1 will be initialized with the value one,
				// resulting: [1,1,...]
				myFibonaziList[i] = 1;
			} else {
				// otherwise we apply the formula: f(x)=f(x-2)+f(x-1)
				myFibonaziList[i] = myFibonaziList[i - 2] + myFibonaziList[i - 1];
			}
		}

		// once we reach the number i=10 the `for` will break and we continue here
		System.out.println(Arrays.toString(myFibonaziList));

	}
}
