# Ljbc 3 Java Data Structures Conditions and Loops

Learn java by challenge lesson 3: learn about the java data structures, conditions and loops. 
In this project you will learn about Arrays and Lists. Additionally you will know about conditional statements with if/else and iterations

## Getting started

### Checkout this Git repository
 * clone the repository `git clone git@gitlab.com:isaac.prz/ljbc-3-java-data-structures-conditions-and-loops.git`
 * import it as eclipse project 
 
### did you know about fibonaci sequence
 * take a look the beauty of how [fibonaci reflects in the nature](https://insteading.com/blog/fibonacci-sequence-in-nature/) 
 * then run the FibonaciGenerator, it's explained in the inline comments (this is a comment `// inline comment` and this also ` /* block or multiline  comment */` ) 
 
```
 import java.util.Arrays;

/**
 * fibonazi numbers are a sequence f(x)=f(x-2)+f(x-1). Where every number is the
 * sum of the two previous numbers the sequence looks like this: 1, 2, 3, 5, 8
 * .... for instance 8 is calculated as the sum of the two previous: (3 + 5)
 *
 */
public class FibonaciGenerator {
	public static void main(String[] args) {
		// final means that the value once assigned in the variable cannot be changed.
		// It's basically a constant.
		final int ARRAY_SIZE = 10;
		// we create an empty array with as much positions as we want
		int myFibonaziList[] = new int[ARRAY_SIZE];

		// this is a bit barroque, but it means, the variable i will start with 0 and
		// end with ARRAY_SIZE and the code inside will be executed every time we add 1
		// to the variable 1
		for (int i = 0; i < ARRAY_SIZE; i++) {
			if (i < 2) {
				// The first two numbers of the fibonaci list are 1. It's just so.
				// This means that the positions 0 and 1 will be initialized with the value one,
				// resulting: [1,1,...]
				myFibonaziList[i] = 1;
			} else {
				// otherwise we apply the formula: f(x)=f(x-2)+f(x-1)
				myFibonaziList[i] = myFibonaziList[i - 2] + myFibonaziList[i - 1];
			}
		}

		// once we reach the number i=10 the "for" will break and we continue here
		System.out.println(Arrays.toString(myFibonaziList));

	}
}

```

![Run v](docs/fibonaci-numbers.png)
 
## Homework

 * learn about arrays in java here you have a couple of resources [java_arrays at w3schools](https://www.w3schools.com/java/java_arrays.asp) [arrays at programiz](https://www.programiz.com/java-programming/arrays)
 * learn about if/else [java_conditions at w3schools](https://www.w3schools.com/java/java_conditions.asp)
 * learn about `for` loops [java_for_loop at w3schools](https://www.w3schools.com/java/java_for_loop.asp)
  * learn about `List` in [java](https://docs.oracle.com/javase/8/docs/api/java/util/List.html) and in particular the [ArrayList implementation](https://www.w3schools.com/java/java_arraylist.asp)
 
## Challenges

1. Generate the first 100 fibonaci numbers (you can use the same FibonaciGenerator.java)
1. Print every fibonaci number in a different line
1. create a new class FibonaciGeneratorWithList.java where instead of using arrays you will use a ArrayList

once finished follow the [upload your exercises documentation](https://gitlab.com/isaac.prz/ljbc-2-java-data-types-input-output#upload-your-exercises) to push them into your branch in the repository.



## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)
